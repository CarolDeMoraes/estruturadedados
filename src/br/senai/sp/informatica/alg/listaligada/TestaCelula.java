package br.senai.sp.informatica.alg.listaligada;

public class TestaCelula {

	public static void main(String[] args) {
	
		Produto p1 = new Produto();
		Produto p2 = new Produto();
		Produto p3 = new Produto();
		
		p1.setNome("Arroz Camil");
		p2.setNome("Bolacha Trakinas");
		p3.setNome("Cebola da Ro�a");
				
		Celula c1 = new Celula(p1);
		Celula c2 = new Celula(p2);
		Celula c3 = new Celula(p3);
		
		// Associando as celulas com os valores
		c3.setProximo(c2);
		c2.setProximo(c1);
		
		System.out.println(c3.getValor().getNome());
		System.out.println(c3.getProximo().getValor().getNome());
	
		// Situa��o antes de remover: c3->c2->c1
		// Se remover...
		
		c3.setProximo(c2.getProximo());
		

	}

	
}
