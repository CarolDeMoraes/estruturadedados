package br.senai.sp.informatica.alg.listaligada;

public class ListaLigada {

	/**
	 * Armazena o primeiro valor
	 */
	private Celula primeiro = null;
	
	/**
	 * Armazena o segundo valor
	 */
	private Celula ultimo = null;
	
	private int tamanho = 0;
	
	/**
	 * Adiciona um novo valor no comeco da lista
	 * @param valor - O valor adicionado
	 */
	public void adicionarNoComeco(Produto valor) {
		// Criando a celula do elemento adicionado
		Celula novo = new Celula(valor);
		
		// Falando que o pr�ximo do novo ser� o antigo primeiro
		novo.setProximo(this.primeiro);
		
		// Definindo que o novo elemento � o primeiro
		this.primeiro = novo;
		
		// Se a lista n�o tem ninguem o novo elemento ser� o ultimo
		if(this.tamanho == 0 ) {
			this.ultimo = novo;
		}
		
		// Aumenta o tamanho
		this.tamanho++;	
	}
	
	public void validarPosicao(int posicao){
		if(posicao>=this.tamanho||posicao<0){
			throw new RuntimeException("Posi��o inv�lida");
		}
	}
	
	public void adicionaNaPosicao(int posicao, Produto valor){
		this.validarPosicao(posicao);
		
		Celula novo = new Celula(valor);
		
		if (posicao == 0) {
			adicionarNoComeco(valor);
		}else if(posicao == this.tamanho-1) {
			adicionarNoFim(valor);
		}else {
			Celula anterior = this.pegarCelula(posicao-1);
			Celula posterior = anterior.getProximo();
			
			anterior.setProximo(novo);
			novo.setProximo(posterior);
		}
		this.tamanho++;
	}		
		
	
	public void  remover(int posicao) {
		
		if(posicao == 0) {
			this.primeiro= this.primeiro.getProximo();
			}else if (posicao == this.tamanho) {
				Celula anterior = this.pegarCelula(posicao-1);
				this.ultimo= anterior;
				}else {
					Celula anterior = this.pegarCelula(posicao-1);
					Celula posterior = anterior.getProximo();
					
					anterior.setProximo(posterior.getProximo());
					
					}
			this.tamanho--;
				}
			




	
	public Celula pegarCelula(int posicao) {
		
		// Cria a celula que ir� iterar o la�o
		// come�ando do primeiro
		Celula iteradora = this.primeiro;
		
		// Pega o primeiro elemento at� a posi��o
		for(int i = 1; i <= posicao; i++) {
			iteradora = iteradora.getProximo();
		}
		return iteradora;
	}
	
	
	public void adicionarNoFim(Produto valor){
		//criar a nova c�lula
		Celula novo = new Celula(valor);
		//se a lista est� vazia a nova c�lula ser� tamb�m a primeira
		if (this.tamanho==0) {
			this.primeiro=novo;
		}
		else {
			//que o �ltimo atual ter� como pr�ximo o novo �ltimo
			this.ultimo.setProximo(novo);
		}
		
		//determinando que a �ltima c�lula � a nova
		this.ultimo=novo;
		
		this.tamanho++;
	}
	

	
	
	/**
	 * Retorna a quantidade de elementos
	 * @return 
	 */
	public int tamanho(){
		return this.tamanho; 
	}
	
	/**
	 * Pega um elemento de dada posi��o
	 * @param posicao
	 * @return
	 */
	public Produto pegar(int posicao) {
		
		// Cria a celula que ir� iterar o la�o
		// come�ando do primeiro
		Celula iteradora = this.primeiro;
		
		// Pega o primeiro elemento at� a posi��o
		for(int i = 1; i <= posicao; i++) {
			iteradora = iteradora.getProximo();
		}
		return iteradora.getValor();
	}
}	
