package br.senai.sp.informatica.alg.listaligada;

import br.senai.sp.informatica.alg.vetor.Cliente;

public class TestaListaLigada {

	public static void main(String[]args) {
		
		Produto p1 = new Produto();
		Produto p2 = new Produto();
		Produto p3 = new Produto();
		Produto p4 = new Produto();
		
		p1.setNome("Arroz Camil");
		p2.setNome("Bolacha Trakinas");
		p3.setNome("Cebola da Ro�a");
		p4.setNome("Guaran� Jesus");
		
		// Criando lista de produtos
		ListaLigada listaDeProdutos = new ListaLigada();
		listaDeProdutos.adicionarNoFim(p1);
		listaDeProdutos.adicionarNoFim(p2);
		listaDeProdutos.adicionarNoFim(p3);
		
		System.out.println(listaDeProdutos.pegar(0).getNome());
		System.out.println(listaDeProdutos.pegar(1).getNome());
		System.out.println(listaDeProdutos.pegar(2).getNome());
		
		
		listaDeProdutos.adicionaNaPosicao(1, p4);
		System.out.println("\nNA POSI��O");
		System.out.println(listaDeProdutos.pegar(0).getNome());
		System.out.println(listaDeProdutos.pegar(1).getNome());
		System.out.println(listaDeProdutos.pegar(2).getNome());
		System.out.println(listaDeProdutos.pegar(3).getNome());
		
	
		listaDeProdutos.remover(1);
		System.out.println("\nESTAMOS REMOVENDO");
		System.out.println(listaDeProdutos.pegar(0).getNome());
		System.out.println(listaDeProdutos.pegar(1).getNome());
		System.out.println(listaDeProdutos.pegar(2).getNome());
		
	}
}
