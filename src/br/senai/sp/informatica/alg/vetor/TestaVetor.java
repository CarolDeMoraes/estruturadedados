package br.senai.sp.informatica.alg.vetor;

public class TestaVetor {

	public static void main(String[]args) {
		Cliente c1 = new Cliente("Adalberto");
		Cliente c2 = new Cliente("Bernardinho");
		Cliente c3 = new Cliente("Dioguitos");
		Cliente c4 = new Cliente("Jesus");
		Cliente c5 = new Cliente("matheus");
		
		Vetor vetorDeClientes = new Vetor();
		vetorDeClientes.adicionar(c1);
		vetorDeClientes.adicionar(c2);
		vetorDeClientes.adicionar(c3);
		vetorDeClientes.adicionar(c4);
		vetorDeClientes.adicionar(c5);
		
		System.out.println(vetorDeClientes.pegar(0));
		System.out.println(vetorDeClientes.pegar(1));
		System.out.println(vetorDeClientes.pegar(2));
		System.out.println(vetorDeClientes.pegar(3));
		System.out.println(vetorDeClientes.pegar(4));
		
	}
}
