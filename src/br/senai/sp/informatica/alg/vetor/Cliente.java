package br.senai.sp.informatica.alg.vetor;

public class Cliente {

	// atributos
	private String nome;

	// construtor
	public Cliente(String nome) {
		this.setNome (nome);
	}

	// m�todo toString (pega o objeto e mostra uma representa��o em texto)
	@Override
	public String toString() {
		return "Cliente [nome=" + nome + "]";
	}

	// getters and setters
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
