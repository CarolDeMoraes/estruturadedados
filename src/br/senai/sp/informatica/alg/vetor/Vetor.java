package br.senai.sp.informatica.alg.vetor;

public class Vetor {

	// criando o vetor para armazenar os dados
	private Object[] dados = new Object[10];

	// armazana a quantidade de elementos do vetor
	private int tamanho = 0;

	/**
	 * Adiciona um elemento no vetor. Se este ultrapassar a capacidade, dobra o
	 * tamanho do vetor de aramazenagem
	 * 
	 * @param valor - O elemento que ser� adicionado
	 */
	public void adicionar(Object valor) {

		// garantindo o tamanho da aloca��o
		this.garantirEspaco();

		// aloca o novo elemento do vetor
		this.dados[this.tamanho] = valor;

		// aumenta a quantidade de elementos adicionados
		this.tamanho++;
	}
	
	/**
	 * Adiciona o valor em um �ndice j� existente do vetor
	 * realocando os elementos j� adicionados caso seja necess�rio
	 * 
	 * @param valor - O valor que ser� adicionado.
	 * @param posicao - O �ndice onde o elemento ser� adicionado.
	 */
	public void adicionarEspecifico(Object valor, int posicao) {
		
		this.checarPosicao(posicao);
		this.garantirEspaco();
		
		// Realocando elementos
		for(int i = this.tamanho;  i > posicao; i--) {
		
		this.dados[i] = this.dados[i-1];	
		}
		this.dados[posicao] = valor;
		this.tamanho++;
		
	}
                                                                                                                                 
	public void remover(int posicao) {
		this.checarPosicao(posicao);

		// remover o elemento da posi��o
		this.dados[posicao] = null;

		for (int i = posicao; i < this.tamanho - 1; i++) {
			dados[i] = dados[i + 1];
			// posicao = posicao + 1;
			dados[i + 1] = null;		
		}
		tamanho--;
	}

	/**
	 * Devolve o tamanho do vetor
	 * 
	 * @return this.tamanho - Retorna o tamanho do vetor
	 */
	public int tamanho() {
		return this.tamanho;
	}

	/**
	 * Devolve uma flag indicando se um dado objeto existe
	 * dentro do vetor
	 * @param valor - O objeto procurado
	 * @return
	 */
	public boolean contem(Object valor) {
		
		// percorre cada elemento do vetor e verificar
		// se � igualao inserido
		for(int i = 0; i < this.tamanho; i++) {
			if(this.dados[i].equals(valor)) {
				return true;
			}
		}
		
		// se n�o encontrou ningu�m, return falso
		return false;
	}

	public Object pegar(int posicao) {

		// v�lida a posi��o
		this.checarPosicao(posicao);

		return this.dados[posicao];
	}

	/**
	 * Checa se a posi��o � v�lida no vetor. Caso n�o seja dispara uma Exception
	 * 
	 * @param posicao
	 *            - A posi��o verificada
	 */
	private void checarPosicao(int posicao) {
		if (posicao < 0 || posicao >= this.tamanho) {
			throw new RuntimeException("A posi��o " + posicao + " � inv�lida");
		}
	}

	/**
	 * verifica se o tamanho do vetor + 1 ultrapassa o limete do vetor de aloca��o.
	 * Caso verdadeiro: Cria um vetor novo com o dobro de tamanho e passa os
	 * elementos do vetor antigo para o novo.
	 */
	private void garantirEspaco() {

		// dobramos o tamanho do vetor se o tamanho l�gico
		// for igual ao f�sico
		if (this.dados.length == this.tamanho) {

			// crinado um vetor com o dobro de capacidade
			Object[] novoVetor = new Object[this.tamanho * 2];

			for (int i = 0; i < this.tamanho; i++) {
				novoVetor[i] = this.dados[i];
			}

			// determine que o novo vetor ser� o vetor de dados
			this.dados = novoVetor;
		}
	}
}
